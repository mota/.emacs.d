;;; helm-passwords.el --- Manage passwords with helm, org-mode and gpg

;; Copyright (C) 2015 Your Name Here

;; Author:  Your Name Here <mota@arch.localdomain>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Set of functions to manage passwords stored in gpg encrypted org
;; file. Passwords can be searched through helm and copied to clipboard

;;; Code:
(require 'helm-org)

;; Append `org-capture' minimal template for capturing passwords
(add-to-list 'org-capture-templates
	     '("p" "Password" entry (file "~/.passwords.gpg")
	       "* %^{Title}\n  %^{URL}p %^{USERNAME}p %^{PASSWORD}p") t)

(defcustom helm-password-manager-wait-time "30 sec"
  "Default period to wait before deleting property from the clipboard."
  :group 'helm-password-manager)

;; Colect list of entries in passwords file
(defun helm-password-manager-get-list ()
  (mapcar
   (lambda (x)
     (cons (substring (car x) 0) (cdr x))) ;; Remove `*' from headlines
   (helm-org--get-candidates-in-file "~/.passwords.gpg" t t)))

;; Define helm source
(setq helm-password-manager-source
      '((name . "Select entry:")
        (candidates . helm-password-manager-get-list)
	(action . (("Copy PASSWORD" . (lambda (candidate)
					(progn
					  (helm-password-manager-actions
					   candidate "PASSWORD"))))
		   ("Copy USERNAME" . (lambda (candidate)
					(progn
					  (helm-password-manager-actions
					   candidate "USERNAME"))))))))

;; Define actions
(defun helm-password-manager-actions (candidate property)
  (progn
    (funcall interprogram-cut-function
	     (org-entry-get candidate property))
    (message (concat property
		     " copied to clipboard and will be removed in "
		     helm-password-manager-wait-time))
    (run-at-time helm-password-manager-wait-time nil
		 (lambda () (funcall interprogram-cut-function "")
		   (message "Clipboard cleared!")))))

;; Close Passwords file
(defun helm-password-manager-finish ()
  (kill-buffer ".passwords.gpg"))

;; Helm function call
(defun helm-password-manager ()
  (interactive)
  (helm :sources '(helm-password-manager-source)
	:buffer "*Helm Password Manager*")
  (helm-password-manager-finish))

(provide 'helm-passwords)
;;; helm-passwords.el ends here
