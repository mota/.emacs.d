function make_ac_matlab_dict()
 
list = {'a' 'b' 'c' 'd' 'e' 'f' 'g' 'h' 'i' 'j' 'k' 'l' 'm' 'n' 'o' ...
        'p' 'q' 'r' 's' 't' 'u' 'v' 'w' 'x' 'y' 'z'};

filename = 'ac-matlab-dict';

fid = fopen(filename, 'a');

matlabMCRprocess = com.mathworks.jmi.MatlabMCR;

for i = list
    data = matlabMCRprocess.mtFindAllTabCompletions(i);

    for i = 1:length(data)
        str = char(data(i));
        fprintf(fid, '%s', str);
        fprintf(fid, '\n');
    end

end

fclose(fid);
