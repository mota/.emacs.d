

;;;;;;;;;;;;;;;;;;;;;;; Real time completions
;; (defun ac-matlab-candidates ()
;;   (with-local-quit
;;     (matlab-shell-completion-list (current-word))))

;; (defvar ac-source-matlab
;;   '((candidates . ac-matlab-candidates)))

;; (defvar ac-source-matlab
;;   '((candidates . ac-matlab-candidates)))

;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'ac-dictionary-directories
	     "~/.emacs.d/elpa/auto-complete-1.4/dict")

(add-hook 'matlab-mode-hook
	  (lambda ()
	    (add-to-list
	     'ac-sources 'ac-source-dictionary)))

(add-hook 'matlab-shell-mode-hook
	  (lambda ()
	    (add-to-list
	     'ac-sources 'ac-source-dictionary)))
