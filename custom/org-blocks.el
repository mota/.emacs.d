(defvar org-block-args nil
  "Arguments inserted automatically by function `org-insert-block' when
creating a block.  Its value is a list of definitions, each element of which
is a list containing two elements: the first is the type of the block and
the second is a strings with the arguments.")
(make-variable-buffer-local 'org-block-args)
(put 'org-block-args 'safe-local-variable 'listp)

(defvar block-history nil
  "History list for quote types.")

;; (setq org-block-args '("matlab" " :session :results silent"))

(defun org-insert-block ()
  "Insert a block interactively with arguments defined in `org-block-args'."
  (interactive)
  (let ((env (completing-read "Language: "
                              '("matlab" "R" "sh" "octave")
                              nil nil nil 'block-history))
        (begin-point (cond (mark-active (region-beginning))
                           (t (point))))
        (body (cond (mark-active
                     (delete-and-extract-region (region-beginning)
                                                (region-end)))
                    (t ""))))
    (goto-char begin-point)
    (insert (concat "\n#+BEGIN_SRC " env
                    (cond (org-block-args
                           (concat " " (car (cdr (assoc env org-block-args)))))
                          (t ""))
                    "\n" body "\n#+END_SRC\n"))
    (previous-line 2)
    (org-edit-special)
    )
  )

;; (eval-after-load "org"
;;   '(progn
;;      (define-key org-mode-map [(control c) (control x) ?#]
;;        'org-insert-block)))

(eval-after-load "org"
  '(progn
     (define-key org-mode-map (kbd "\C-c i")
       'org-insert-block)))

(provide 'org-blocks)
