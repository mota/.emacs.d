;; This code saves a list of packages installed via package.el.

;; Function to save VARLIST to FILENAME
(defun dump-vars-to-file (varlist filename)
  "simplistic dumping of variables in VARLIST to a file FILENAME"
  (save-excursion
    (let ((buf (find-file-noselect filename)))
      (set-buffer buf)
      (erase-buffer)
      (dump varlist buf)
      (save-buffer)
      (kill-buffer))))

(defun dump (varlist buffer)
  "insert into buffer the setq statement to recreate the variables in VARLIST"
  (loop for var in varlist do
        (print (list 'setq var (list 'quote (symbol-value var)))
               buffer)))

(defun remember-packages()
  (package-initialize)
  (setq package-list (mapcar #'(lambda (x) (car x)) package-alist))
  (dump-vars-to-file '(package-list) "~/.emacs.d/installed-packages")
  )

(provide 'remember-packages)
