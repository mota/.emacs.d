(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(android-mode-sdk-dir "~/Apps/android-sdk-linux")
 '(ansi-color-names-vector
   ["#3C3836" "#FB4934" "#B8BB26" "#FABD2F" "#83A598" "#D3869B" "#8EC07C" "#EBDBB2"])
 '(confirm-kill-emacs (quote y-or-n-p))
 '(custom-safe-themes
   (quote
    ("cb352689ac005b70628dde1c4bd618472d83c9850551cf07adb683a2d087bfda" "4078e9861774de971222af675577800b6f8a5a18e7c3b7670e97c568506b5677" default)))
 '(display-time-24hr-format t)
 '(globalff-adaptive-selection t)
 '(globalff-matching-filename-limit 500)
 '(globalff-search-delay 0)
 '(ido-decorations
   (quote
    ("{" "}" " | " " | ..." "[" "]" " [No match]" " [Matched]" " [Not readable]" " [Too big]" " [Confirm]")))
 '(inhibit-startup-screen t)
 '(magit-diff-arguments (quote ("--no-ext-diff")))
 '(matlab-shell-command-switches (quote ("-nodesktop -nosplash")))
 '(notmuch-saved-searches
   (quote
    ((:name "inbox" :query "tag:inbox")
     (:name "unread" :query "tag:unread"))))
 '(org-agenda-files nil)
 '(org-publish-use-timestamps-flag nil)
 '(org-support-shift-select t)
 '(package-selected-packages
   (quote
    (magit-gitflow pipenv jupyter lsp-dart yaml-mode idea-darkula-theme darcula-theme flutter dart-mode dart-server cmake-mode helm-swoop cider ssh-agency json-navigator blimp eglot perspective company-anaconda doom-modeline tide use-package typescript-mode company-quickhelp rjsx-mode company-tern indium eldoc-eval shrink-path evil-tutor key-chord syntax-subword hardcore-mode academic-phrases org-noter helm-rg zerodark-theme exec-path-from-shell ivy-bibtex org-plus-contrib org-wc counsel tramp-term org-pdfview yafolding xml-gen which-key web-mode waher-theme virtualenv unfill typing-practice typing trr synonyms swiper sudo-edit speed-type speck smartscan smartparens shorten semi rainbow-mode rainbow-delimiters python-mode popwin paredit orgbox org-page org-bullets oauth nyan-mode notmuch nose nlinum navi-mode names multiple-cursors multi-term modalka minimap markdown-mode manage-minor-mode magit lui leuven-theme legalese lcs latex-extra keywiz json-mode ipython impatient-mode iedit idomenu hydra hideshow-org helm-projectile helm-make helm-bibtex helm-ag header2 haskell-mode grizzl google-translate god-mode git-timemachine git-gutter fzf fuzzy framemove flycheck flx-ido fancy-narrow exwm expand-region evil esup ess-R-data-view eshell-prompt-extras eshell-git-prompt es-lib elpy elnode ebib dired-sort dired-narrow define-word dedicated conkeror-minor-mode company-lua color-theme back-button babel autopair android-mode aggressive-indent ace-jump-mode ac-octave ac-helm)))
 '(rw-hunspell-default-dictionary "en_US_hunspell")
 '(rw-hunspell-dicpath-list (quote ("/usr/share/hunspell")))
 '(rw-hunspell-make-dictionary-menu t)
 '(rw-hunspell-use-rw-ispell t)
 '(safe-local-variable-values
   (quote
    ((eval flyspell-mode)
     (org-odd-levels-only . t)
     (org-support-shift-select . t)
     (org-babel-min-lines-for-block-output . 100)
     (org-confirm-babel-evaluate)
     (org-export-latex-listings . t)
     (org-export-latex-packages-alist))))
 '(show-paren-mode t)
 '(text-mode-hook (quote (turn-on-auto-fill text-mode-hook-identify))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t)))
 '(globalff-selection-face ((t (:background "dark olive green" :foreground "light goldenrod" :underline nil))) t)
 '(highlight-indentation-face ((t (:inherit fringe :background "dim grey"))))
 '(ido-first-match ((t (:foreground "#73c936" :weight bold))))
 '(ido-subdir ((t (:foreground "#cc8c3c"))))
 '(org-agenda-structure ((t (:foreground "RoyalBlue1"))))
 '(org-scheduled-previously ((t (:foreground "goldenrod"))))
 '(speck-doublet ((t (:underline (:color "brown" :style wave)))))
 '(speck-guess ((t (:underline (:color "red" :style wave)))))
 '(speck-miss ((t (:underline (:color "orange" :style wave)))))
 '(speck-mouse ((t (:foreground "orange red"))))
 '(speck-query ((t (:foreground "orange red")))))
