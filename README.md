Overview
========

Those are my Emacs settings and customisations. Fell free to try it
out. I will try to keep everything as tidy as possible, but I'm still
learning how to do it, so I can't promise anything.


Notes
-----

*ido* and *smex* keep track of last used files/commands in .ido.last
and .smex-items, both save on HOME dir. I'm not sure if this it's the
best place for it. In the future, maybe I should change then to some
place in .emacs.d dir.

<s>Elpa's archive-contents shoud be in the repo to remember-packages to
work.</s> Actually, what we have to do is probably to use
*package-refresh-contents* before updating packages.

TODO
----
