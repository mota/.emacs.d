;;; adwaita-dark-theme.el --- colour theme for Emacs, based on emacs-theme-darktooth

;; Authors:
;; URL:
;; Version:

;;; Commentary:

;; A remix of emacs-theme-darktooth to be used with Gnome's Adwaita Dark Theme

;;; Code:
(deftheme adwaita-dark "A remix of emacs-theme-darktooth to be
used with Gnome's Adwaita Dark Theme")

(let (
      (ad-dark0_hard		"#1D2021")
      (ad-dark0			"#2F343F")
      (ad-dark0_soft		"#32302F")
      (ad-dark1			"#3C3836")
      (ad-dark2			"#4D5559")
      (ad-dark3			"#665C54")
      (ad-dark4			"#7C7C7C")
      (ad-medium		"#B3B3B3")

      (ad-light0_hard		"#FFFFC8")
      (ad-light0		"#FDF4C1")
      (ad-light0_soft		"#F4E8BA")
      (ad-light1		"#EBDBB2")
      (ad-light2		"#D5C4A1")
      (ad-light3		"#BDAE93")
      (ad-light4		"#8F8270")

      (ad-bright_red		"#FB4933")
      (ad-bright_green		"#B8BB26")
      (ad-bright_yellow		"#FABD2F")
      (ad-bright_blue		"#83A598")
      (ad-bright_purple		"#D3869B")
      (ad-bright_aqua		"#8EC07C")
      (ad-bright_orange		"#FE8019")

      ;; neutral, no 256-color code, requested, nice work-around meanwhile
      (ad-neutral_red		"#FB4934"); "#D75F5F")
      (ad-neutral_green		"#B8BB26"); "#AFAF00")
      (ad-neutral_yellow	"#FABD2F"); "#FFAF00")
      (ad-neutral_blue		"#83A598"); "#87AFAF")
      (ad-neutral_purple	"#D3869B"); "#D787AF")
      (ad-neutral_aqua		"#8EC07C"); "#87AF87")
      (ad-neutral_orange	"#FE8019"); "#FF8700")

      (ad-faded_red		"#9D0006")
      (ad-faded_green		"#79740E")
      (ad-faded_yellow		"#D18817")
      (ad-faded_blue		"#076678")
      (ad-faded_purple		"#8F3F71")
      (ad-faded_aqua		"#427B58")
      (ad-faded_orange		"#AF3A03")

      (ad-delimiter-one		"#5C7E81")
      (ad-delimiter-two		"#837486")
      (ad-delimiter-three	"#9C6F68")
      (ad-delimiter-four	"#7B665C")

      (ad-white			"#FFFFFF")
      (ad-black			"#000000")
      (ad-sienna		"#DD6F48")
      (ad-darkslategray4	"#528B8B")
      (ad-lightblue4		"#66999D")
      (ad-burlywood4		"#BBAA97")
      (ad-aquamarine4		"#83A598")
      (ad-turquoise4		"#61ACBB")
      )

  (custom-theme-set-faces
   'adwaita-dark

   ;; UI
   `(default ((t (:foreground ,ad-light0 :background ,ad-dark0))))
   `(cursor  ((t (:background ,ad-light0))))
   `(vertical-border ((t (:foreground ,ad-dark2 ))))

   `(mode-line ((t (:foreground ,ad-light1 :background ,ad-dark2
				:box (:line-width 4 :color ,ad-dark2)
				))))
   `(mode-line-inactive ((t (:foreground ,ad-light4 :background ,ad-dark2
					 :box (:line-width 4 :color ,ad-dark2)
					 ))))
   `(fringe ((t (:foreground, ad-dark2 :background, ad-dark0 ))))
   `(linum ((t (:foreground ,ad-dark4 ))))
   `(hl-line ((t (:background ,"#42494D" ))))
   `(region     ((t (:background ,"#5E676B")))) ;;selection
   `(secondary-selection ((t (:background ,"#42494D"))))
   `(minibuffer-prompt
     ((t
       (:foreground ,ad-turquoise4 :background ,ad-dark0 :bold nil))))

   ;; Built-in syntax
   `(font-lock-builtin-face ((t (:foreground ,ad-bright_orange))))
   `(font-lock-constant-face ((t (:foreground ,ad-burlywood4))))
   `(font-lock-comment-face ((t (:foreground ,ad-faded_yellow))))
   `(font-lock-function-name-face  ((t (:foreground ,ad-light4))))
   `(font-lock-keyword-face ((t (:foreground ,ad-sienna))))
   `(font-lock-string-face ((t (:foreground ,ad-darkslategray4))))
   `(font-lock-variable-name-face  ((t (:foreground ,ad-aquamarine4))))
   `(font-lock-type-face   ((t (:foreground ,ad-lightblue4))))
   `(font-lock-warning-face   ((t (:foreground ,ad-neutral_red :bold t))))

   ;; whitespace-mode
   `(whitespace-space ((t (:foreground ,ad-dark4 :background ,ad-neutral_red))))
   `(whitespace-hspace ((t (:foreground ,ad-dark4 :background ,ad-neutral_red))))
   `(whitespace-tab ((t (:foreground ,ad-dark4 :background ,ad-neutral_red))))
   `(whitespace-newline ((t (:foreground ,ad-dark4 :background ,ad-neutral_red))))
   `(whitespace-trailing ((t (:foreground ,ad-neutral_red :background ,ad-neutral_red))))
   `(whitespace-line ((t (:foreground ,ad-neutral_red :background ,ad-neutral_red))))
   `(whitespace-space-before-tab ((t (:foreground ,ad-dark4 :background ,ad-neutral_red))))
   `(whitespace-indentation ((t (:foreground ,ad-dark4 :background ,ad-neutral_red))))
   `(whitespace-empty ((t (:foreground nil :background ,ad-neutral_red))))
   `(whitespace-space-after-tab ((t (:foreground ,ad-dark4 :background ,ad-neutral_red))))

   ;; RainbowDelimiters
   `(rainbow-delimiters-depth-1-face  ((t ( :foreground ,ad-delimiter-one    ))))
   `(rainbow-delimiters-depth-2-face  ((t ( :foreground ,ad-delimiter-two    ))))
   `(rainbow-delimiters-depth-3-face  ((t ( :foreground ,ad-delimiter-three  ))))
   `(rainbow-delimiters-depth-4-face  ((t ( :foreground ,ad-delimiter-four   ))))
   `(rainbow-delimiters-depth-5-face  ((t ( :foreground ,ad-delimiter-one    ))))
   `(rainbow-delimiters-depth-6-face  ((t ( :foreground ,ad-delimiter-two    ))))
   `(rainbow-delimiters-depth-7-face  ((t ( :foreground ,ad-delimiter-three  ))))
   `(rainbow-delimiters-depth-8-face  ((t ( :foreground ,ad-delimiter-four   ))))
   `(rainbow-delimiters-depth-9-face  ((t ( :foreground ,ad-delimiter-one    ))))
   `(rainbow-delimiters-depth-10-face  ((t ( :foreground ,ad-delimiter-two   ))))
   `(rainbow-delimiters-depth-11-face  ((t ( :foreground ,ad-delimiter-three ))))
   `(rainbow-delimiters-depth-12-face  ((t ( :foreground ,ad-delimiter-four  ))))
   `(rainbow-delimiters-unmatched-face ((t ( :foreground ,ad-light0 :background nil ))))

   ;; linum-relative
   `(linum-relative-current-face ((t ( :foreground ,ad-light4 :background ,ad-dark1 ))))

   ;; Highlight indentation mode
   `(highlight-indentation-current-column-face ((t ( :background ,ad-dark2 ))))
   `(highlight-indentation-face ((t ( :background ,ad-dark1 ))))

   ;; Smartparens
   `(sp-pair-overlay-face ((t ( :background ,ad-dark2 ))))
   ;;`(sp-wrap-overlay-face ((t (:inherit sp-wrap-overlay-face))))
   ;;`(sp-wrap-tag-overlay-face ((t (:inherit sp-wrap-overlay-face))))
   `(sp-show-pair-match-face ((t ( :background ,ad-dark2)))) ;; Pair tags highlight
   `(sp-show-pair-mismatch-face   ((t ( :background ,ad-neutral_red)))) ;; Highlight for bracket without pair

   ;; elscreen
   `(elscreen-tab-background-face  ((t (:background ,ad-dark0 :box nil)))) ;; Tab bar, not the tabs
   `(elscreen-tab-control-face   ((t ( :foreground ,ad-neutral_red :background ,ad-dark2 :box nil :underline nil)))) ;; The controls
   `(elscreen-tab-current-screen-face  ((t ( :foreground ,ad-dark0 :background ,ad-dark4 :box nil)))) ;; Current tab
   `(elscreen-tab-other-screen-face  ((t ( :foreground ,ad-light4 :background ,ad-dark2 :box nil :underline nil)))) ;; Inactive tab

   ;; ag (The Silver Searcher)
   `(ag-hit-face ((t ( :foreground ,ad-neutral_blue ))))
   `(ag-match-face ((t ( :foreground ,ad-neutral_red ))))

   ;; Diffs
   `(diff-changed ((t ( :foreground ,ad-light1 :background nil ))))
   `(diff-added ((t ( :foreground ,ad-neutral_green :background nil ))))
   `(diff-removed ((t ( :foreground ,ad-neutral_red :background nil ))))
   `(diff-indicator-changed ((t (:inherit diff-changed ))))
   `(diff-indicator-added ((t (:inherit diff-added ))))
   `(diff-indicator-removed ((t (:inherit diff-removed ))))

   `(js2-warning ((t (:underline (:color ,ad-bright_yellow :style wave) ))))
   `(js2-error ((t (:underline (:color ,ad-bright_red :style wave) ))))
   `(js2-external-variable ((t (:underline (:color ,ad-bright_aqua :style wave) ))))
   `(js2-jsdoc-tag ((t ( :foreground ,ad-medium :background nil ))))
   `(js2-jsdoc-type ((t ( :foreground ,ad-light4 :background nil ))))
   `(js2-jsdoc-value ((t ( :foreground ,ad-light3 :background nil ))))
   `(js2-function-param ((t ( :foreground ,ad-bright_aqua :background nil ))))
   `(js2-function-call ((t ( :foreground ,ad-bright_blue :background nil ))))
   `(js2-instance-member ((t ( :foreground ,ad-bright_orange :background nil ))))
   `(js2-private-member ((t ( :foreground ,ad-faded_yellow :background nil ))))
   `(js2-private-function-call ((t ( :foreground ,ad-faded_aqua :background nil ))))
   `(js2-jsdoc-html-tag-name ((t ( :foreground ,ad-light4 :background nil ))))
   `(js2-jsdoc-html-tag-delimiter ((t ( :foreground ,ad-light3 :background nil ))))

   ;; org-mode
   `(org-agenda-date-today ((t ( :foreground ,ad-light2 :slant italic :weight bold))) t)
   `(org-agenda-structure ((t (:inherit font-lock-comment-face ))))
   `(org-archived ((t ( :foreground ,ad-light0 :weight bold ))))
   `(org-checkbox ((t ( :foreground ,ad-light2 :background ,ad-dark0 :box (:line-width 1 :style released-button) ))))
   `(org-date ((t ( :foreground ,ad-turquoise4 :underline t ))))
   `(org-deadline-announce ((t ( :foreground ,ad-faded_red ))))
   `(org-done ((t ( :foreground ,ad-bright_green :bold t :weight bold ))))
   `(org-formula ((t ( :foreground ,ad-bright_yellow ))))
   `(org-headline-done ((t ( :foreground ,ad-bright_green ))))
   `(org-hide ((t ( :foreground ,ad-dark0 ))))
   `(org-level-1 ((t ( :foreground ,ad-neutral_blue ))))
   `(org-level-2 ((t ( :foreground ,ad-bright_green ))))
   `(org-level-3 ((t ( :foreground ,ad-bright_yellow ))))
   `(org-level-4 ((t ( :foreground ,ad-bright_red ))))
   `(org-level-5 ((t ( :foreground ,ad-faded_aqua ))))
   `(org-level-6 ((t ( :foreground ,ad-bright_green ))))
   `(org-level-7 ((t ( :foreground ,ad-bright_blue ))))
   `(org-level-8 ((t ( :foreground ,ad-bright_blue ))))
   `(org-link ((t ( :foreground ,ad-bright_yellow :underline t ))))
   `(org-scheduled ((t ( :foreground ,ad-bright_green ))))
   `(org-scheduled-previously ((t ( :foreground ,ad-bright_red ))))
   `(org-scheduled-today ((t ( :foreground ,ad-bright_blue ))))
   `(org-sexp-date ((t ( :foreground ,ad-bright_blue :underline t ))))
   `(org-special-keyword ((t ( :inherit font-lock-comment-face ))))
   `(org-table ((t ( :foreground ,ad-bright_green ))))
   `(org-tag ((t ( :bold t :weight bold ))))
   `(org-time-grid ((t ( :foreground ,ad-bright_orange ))))
   `(org-todo ((t ( :foreground ,ad-bright_red :weight bold :bold t ))))
   `(org-upcoming-deadline ((t ( :inherit font-lock-keyword-face ))))
   `(org-warning ((t ( :foreground ,ad-bright_red :weight bold :underline nil :bold t ))))
   `(org-column ((t ( :background ,ad-dark0 ))))
   `(org-column-title ((t ( :background ,ad-dark0_hard :underline t :weight bold ))))
   `(org-mode-line-clock ((t ( :foreground ,ad-light2 :background ,ad-dark0 ))))
   `(org-mode-line-clock-overrun ((t ( :foreground ,ad-black :background ,ad-bright_red ))))
   `(org-ellipsis ((t ( :foreground ,ad-bright_yellow :underline t ))))
   `(org-footnote ((t ( :foreground ,ad-faded_aqua :underline t ))))

   ;; powerline
   `(powerline-active1    ((t (:background ,ad-dark2 :inherit mode-line))))
   `(powerline-active2    ((t (:background ,ad-dark1 :inherit mode-line))))
   `(powerline-inactive1   ((t (:background ,ad-medium :inherit mode-line-inactive))))
   `(powerline-inactive2   ((t (:background ,ad-dark2 :inherit mode-line-inactive))))

   ;; helm
   `(helm-M-x-key ((t ( :foreground ,ad-neutral_orange ))))
   `(helm-action ((t ( :foreground ,ad-white :underline t ))))
   `(helm-bookmark-addressbook ((t ( :foreground ,ad-neutral_red ))))
   `(helm-bookmark-directory ((t ( :foreground ,ad-bright_purple ))))
   `(helm-bookmark-file ((t ( :foreground ,ad-faded_blue ))))
   `(helm-bookmark-gnus ((t ( :foreground ,ad-faded_purple ))))
   `(helm-bookmark-info ((t ( :foreground ,ad-turquoise4 ))))
   `(helm-bookmark-man ((t ( :foreground ,ad-sienna ))))
   `(helm-bookmark-w3m ((t ( :foreground ,ad-neutral_yellow ))))
   `(helm-buffer-directory ((t ( :foreground ,ad-white :background ,ad-bright_blue ))))
   `(helm-buffer-not-saved ((t ( :foreground ,ad-faded_red ))))
   `(helm-buffer-process ((t ( :foreground ,ad-burlywood4 ))))
   `(helm-buffer-saved-out ((t ( :foreground ,ad-bright_red ))))
   `(helm-buffer-size ((t ( :foreground ,ad-bright_purple ))))
   `(helm-candidate-number ((t ( :foreground ,ad-neutral_green ))))
   `(helm-ff-directory ((t ( :foreground ,ad-neutral_purple ))))
   `(helm-ff-executable ((t ( :foreground ,ad-turquoise4 ))))
   `(helm-ff-file ((t ( :foreground ,ad-sienna ))))
   `(helm-ff-invalid-symlink ((t ( :foreground ,ad-white :background ,ad-bright_red ))))
   `(helm-ff-prefix ((t ( :foreground ,ad-black :background ,ad-neutral_yellow ))))
   `(helm-ff-symlink ((t ( :foreground ,ad-neutral_orange ))))
   `(helm-grep-cmd-line ((t ( :foreground ,ad-neutral_green ))))
   `(helm-grep-finish ((t ( :foreground ,ad-turquoise4 ))))
   `(helm-grep-file ((t ( :foreground ,ad-faded_purple ))))
   `(helm-grep-lineno ((t ( :foreground ,ad-neutral_orange ))))
   `(helm-grep-match ((t ( :foreground ,ad-neutral_yellow ))))
   `(helm-grep-running ((t ( :foreground ,ad-neutral_red ))))
   `(helm-header ((t ( :foreground ,ad-aquamarine4 ))))
   `(helm-helper ((t ( :foreground ,ad-aquamarine4 ))))
   `(helm-history-deleted ((t ( :foreground ,ad-black :background ,ad-bright_red ))))
   `(helm-history-remote ((t ( :foreground ,ad-faded_red ))))
   `(helm-lisp-completion-info ((t ( :foreground ,ad-faded_orange ))))
   `(helm-lisp-show-completion ((t ( :foreground ,ad-bright_red ))))
   `(helm-locate-finish ((t ( :foreground ,ad-white :background ,ad-aquamarine4 ))))
   `(helm-match ((t ( :foreground ,ad-neutral_orange ))))
   `(helm-moccur-buffer ((t ( :foreground ,ad-bright_aqua :underline t ))))
   `(helm-prefarg ((t ( :foreground ,ad-turquoise4 ))))
   `(helm-selection ((t ( :foreground ,ad-white :background ,"#42494D" ))))
   `(helm-selection-line ((t ( :foreground ,ad-white :background ,ad-dark2 ))))
   `(helm-separator ((t ( :foreground ,ad-faded_red ))))
   `(helm-source-header ((t ( :foreground ,ad-light2 ))))
   `(helm-visible-mark ((t ( :foreground ,ad-black :background ,ad-light3 ))))

   ;; Term
   `(term-color-black ((t ( :foreground ,ad-dark1 ))))
   `(term-color-blue ((t ( :foreground ,ad-neutral_blue ))))
   `(term-color-cyan ((t ( :foreground ,ad-neutral_aqua ))))
   `(term-color-green ((t ( :foreground ,ad-neutral_green ))))
   `(term-color-magenta ((t ( :foreground ,ad-neutral_purple ))))
   `(term-color-red ((t ( :foreground ,ad-neutral_red ))))
   `(term-color-white ((t ( :foreground ,ad-light1 ))))
   `(term-color-yellow ((t ( :foreground ,ad-neutral_yellow ))))
   `(term-default-fg-color ((t ( :foreground ,ad-light0 ))))
   `(term-default-bg-color ((t ( :background ,ad-dark0) )))

   ;; Message mode
   `(message-cited-text ((t ( :foreground ,ad-medium)))))

  (custom-theme-set-variables 'adwaita-dark
			      `(ansi-color-names-vector
				    [,ad-dark1 ,ad-neutral_red
				    ,ad-neutral_green
				    ,ad-neutral_yellow
				    ,ad-neutral_blue
				    ,ad-neutral_purple
				    ,ad-neutral_aqua
				    ,ad-light1])))

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'adwaita-dark)
;;; adwaita-dark-theme.el ends here
